/**
 * Created by lichong on 3/7/2018.
 */
import React from 'react';

import { Table, Row ,Icon, Divider, Dropdown, InputNumber, Menu, Col,Button } from 'antd';

const menu = (
    <Menu>
        <Menu.Item>
            <a href="javascript:void(0)">1st menu item</a>
        </Menu.Item>
        <Menu.Item>
            <a href="javascript:void(0)">2nd menu item</a>
        </Menu.Item>
        <Menu.Item>
            <a href="javascript:void(0)">3rd menu item</a>
        </Menu.Item>
    </Menu>
);

const columns = [{
    title: '商品',
    dataIndex: 'name',
    key: 'name',
    width: 400,
    render : (text, record) => {
        return (
            <div>
                <Col span={9}>
                    <img width="120" src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"/>
                </Col>
                <Col span={8}>
                    <span className="ant-form-text" style={{fontSize : '14px'}}>你好啊啊啊啊啊啊啊</span><br/>
                </Col>
                <Col span={7}>
                    <span className="ant-form-text" style={{fontSize : '10px' , marginLeft:'10px' , color:'#666'}}>颜色: 红色</span><br/>
                    <span className="ant-form-text" style={{fontSize : '10px' , marginLeft:'10px' , color:'#666'}}>颜色: 红色</span><br/>
                </Col>
            </div>
        );
    }
}, {
    title: '单价',
    dataIndex: 'age',
    key: 'age',
    width: 100,
    render : (text, record) => {
        return (
            <div>
                <span className="ant-form-text">$2999</span><br/>
            </div>
        );
    }
}, {
    title: '数量',
    dataIndex: 'address',
    key: 'address',
    width: 150,
    render : (text, record) => {
        return (
            <div>
                <span className="ant-form-text">x 2</span><br/>
            </div>
        )
    }
}, {
    title: '小计',
    dataIndex: 'subtotal',
    key: 'subtotal',
    width: 100,
    render : (text, record) => {
        return (
            <div>
                <span className="ant-form-text" style={{fontSize : '10px' , marginLeft:'10px' , color:'#666'}}>¥25.80</span><br/>
            </div>
        )
    }
}];

const data = [];
for (let i = 1; i <= 5; i++) {
    data.push({
        key: i,
        name: 'John Brown',
        age: `${i}2`,
        address: `New York No. ${i} Lake Park`,
        description: `My name is John Brown, I am ${i}2 years old, living in New York No. ${i} Lake Park.`,
        promotion : 'lalala'
    });
}

class ItemList extends React.Component {
    state = {
        bordered: false,
        loading: false,
        pagination: false,
        size: 'default',
        showHeader : true,
        footer : false,
        rowSelection: false,
        scroll: undefined,
    }

    render() {
        return (
            <div>
                <Row>
                    <Table {...this.state} columns={columns} dataSource={data} />
                </Row>
                <Row>
                    <div>
                        <p className="pull-right">
                            商品总额：¥264.00
                            返　　现：-¥0.00
                            运　　费： ¥0.00
                            应付总额：¥264.00
                        </p>
                    </div>
                </Row>
            </div>
        );
    }
}
export default ItemList;