/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Input, Divider, Button, Form, Rate, message} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';
import HttpUtils from "../../utils/HttpUtils";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

class CreateComment extends React.Component {

    state = {
        items: [
            {},
        ],
        commentMap:{}
    }

    componentDidMount(){
        let self = this;
        let orderId = HttpUtils.getQueryString('orderId');
        HttpUtils.listSurveys(orderId , {
            success(resp){
                self.setState({items : resp.data})
            } ,
            error(resp){

            }
        })
    }
    handleSubmit(surveyItem) {
        let itemId = surveyItem.prodId;
        let commentMap = this.state.commentMap;

        let rate = commentMap[itemId].rate;
        let content = commentMap[itemId].content;

        let data = {
            orderItemId : surveyItem.orderItemId,
            rate,
            content
        };

        let self = this;
        message.loading('保存中', 0);
        HttpUtils.createComment(data, {
            success(resp){
                message.destroy()
                let oldItems = self.state.items;
                let newItems = oldItems.filter((item) => {
                    return item.orderItemId !== surveyItem.orderItemId
                })
                self.setState({items : newItems})
            },
            error(resp) {
                message.error('error happened')
            }
        })

        // this.props.form.validateFields((err, values) => {
        //     if (err) {
        //         console.log('Received values of form: ', values);
        //         return;
        //     }
        //     let data = values;
        //     message.loading('保存中', 0);
        //     HttpUtils.createComment(data, {
        //         success(resp){
        //             message.destroy()
        //         },
        //         error(resp) {
        //             message.error('error happened')
        //         }
        //     })
        // })
    }

    onRateChanged(surveyItem , value){
        let rateField = "item" + surveyItem.prodId + "rate";
        let commentMap = this.state.commentMap;
        commentMap[surveyItem.prodId] || (commentMap[surveyItem.prodId] = {});
        commentMap[surveyItem.prodId].rate = value;
        this.setState({commentMap})
    }

    onContentChanged(surveyItem , e){
        let contentField = "item" + surveyItem.prodId + "content";
        let commentMap = this.state.commentMap;
        commentMap[surveyItem.prodId] || (commentMap[surveyItem.prodId] = {});
        commentMap[surveyItem.prodId].content = e.target.value;
        this.setState({commentMap})
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row style={{backgroundColor: '#fbfbfb'}}>
                            <Col span={8}>
                                <strong>物流服务评价</strong>
                            </Col>
                            <Col span={16}>
                                <span style={{marginLeft: '20px'}}>
                                    快递包装<Rate />
                                </span>
                                <span style={{marginLeft: '20px'}}>
                                    送货速度<Rate />
                                </span>
                                <span style={{marginLeft: '20px'}}>
                                    配送员态度<Rate />
                                </span>
                            </Col>
                        </Row>
                        {
                            this.state.items
                            &&
                            this.state.items.map((item, index) => {
                                return (
                                    <Row style={{marginTop: '10px', backgroundColor: '#fbfbfb'}} key={index}>
                                        <Col span={8} style={{borderRight: '1px solid'}} className='text-center'>
                                            <img width="200"
                                                 src={item.prodImg}/>
                                            <p>
                                                {item.prodName}
                                            </p>
                                            <p>{item.dealSubTotal}</p>
                                        </Col>
                                        <Col span={16}>
                                            <Row gutter={30} style={{marginTop:'20px'}}>
                                                <Col span={6}>
                                                    <span className="pull-right">商品评分</span>
                                                </Col>
                                                <Col span={18}>
                                                    <Rate ref={"item" + item.prodId + ".rate"} onChange={(e) => this.onRateChanged(item , e)}/>
                                                </Col>
                                            </Row>
                                            <Row gutter={30} style={{marginTop:'20px'}}>
                                                <Col span={6}>
                                                    <span className="pull-right">评论内容</span>
                                                </Col>
                                                <Col span={18}>
                                                    <TextArea onChange={(e) => this.onContentChanged(item , e)}/>
                                                </Col>
                                            </Row>
                                            <Row gutter={30} style={{marginTop:'20px'}}>
                                                <Col span={6}>
                                                </Col>
                                                <Col span={18}>
                                                    <Button type="primary" onClick={() => this.handleSubmit(item)} style={{width: '200px'}}>提交评论</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                )
                            })
                        }
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default CreateComment;